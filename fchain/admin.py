from django.contrib import admin
from fchain.models import Term, Rule, Tree, Person, Sentence

class TermAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'link',
    )

class RuleAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'term1',
        'term2',
        'term1_alike',
        'term2_alike',
        'conclusion',
    )

class SentenceAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'person1',
        'person2',
        'term',
        'is_displayed',
        'tree',
    )

class TreeAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
    )

class PersonAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
    )

# Register your models here.
admin.site.register(Term, TermAdmin)
admin.site.register(Rule, RuleAdmin)
admin.site.register(Tree, TreeAdmin)
admin.site.register(Person, PersonAdmin)
admin.site.register(Sentence, SentenceAdmin)
