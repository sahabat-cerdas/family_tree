from django.urls import path

from fchain.views import index, tree_detail, create_tree, delete_tree, \
        add_anggota, add_anggota_pasangan, cari_relasi

urlpatterns = [
    path('', index, name='index'),
    path('tree/', tree_detail, name='tree_detail'),
    path('new_tree/', create_tree, name='create_tree'),
    path('delete_tree/', delete_tree, name='delete_tree'),
    path('add_anggota/', add_anggota, name='add_anggota'),
    path('add_anggota_pasangan/', add_anggota_pasangan,
            name='add_anggota_pasangan'),
    path('cari_relasi/', cari_relasi, name='cari_relasi'),
]
