import os
import csv
from django.contrib import messages
from django.conf import settings
from django.shortcuts import render, redirect, reverse
from fchain.models import Term, Rule, Sentence, Person, Tree
from fchain.forward_chain import forward_chain
from fchain.anggota_form import AnggotaForm
from fchain.forms import ChoiceForm
from fchain.form_pasangan import PasanganForm
import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

response = {}
choices_list = ()


def populate_database():

    rule_file = os.path.join(settings.BASE_DIR, 'csv/rules.csv')
    term_file = os.path.join(settings.BASE_DIR, 'csv/terms.csv')

    if not Term.objects.all().count():
        with open(term_file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                term = Term.objects.create(link=row[0])

    if not Rule.objects.all().count():
        with open(rule_file) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                term1 = Term.objects.filter(link=row[0])[0]
                term2 = Term.objects.filter(link=row[1])[0]
                conclusion = Term.objects.filter(link=row[4])[0]

                term1_alike = Rule.INDEX_CHOICES[int(row[2])][0]
                term2_alike = Rule.INDEX_CHOICES[int(row[3])][0]

                rule = Rule.objects.create(
                    term1_id=term1.id,
                    term2_id=term2.id,
                    term1_alike=term1_alike,
                    term2_alike=term2_alike,
                    conclusion_id=conclusion.id
                )


def index(request):
    populate_database()
    tree = Tree.objects.all()
    response = {'tree': tree}
    html = 'index.html'
    return render(request, html, response)


def create_tree(request):

    if request.method == 'POST' \
            and len(Tree.objects.filter(name=request.POST['treename'])) == 0:

        tree_desc = request.POST['granpa']+" "+request.POST['granma']
        tree = Tree.objects.create(
            name=request.POST['treename'],
            desc=tree_desc)
        tree.save()

        granpa = Person.objects.create(name=request.POST['granpa'],
                                       gender='M',
                                       tree=tree)
        granpa.save()

        sentence_save(granpa, "male", tree)

        granma = Person.objects.create(name=request.POST['granma'],
                                       gender='F',
                                       spouse=granpa,
                                       tree=tree)
        granma.save()

        sentence_save(granma, "female", tree)

        granpa.spouse = granma
        granpa.save()

        sentence_save_(granpa, granma, "husband", tree)
        sentence_save_(granma, granpa, "wife", tree)

        messages.success(request, "Pohon Keluarga Berhasil Ditambahkan")
    else:
        messages.warning(request, "Pohon Keluarga Tidak Berhasil Ditambahkan")

    return redirect(reverse('index'))


def delete_tree(request):
    id = request.GET.get('id', None)

    if id is not None:
        tree = Tree.objects.get(id=id)
        tree.delete()
        messages.success(request, "Pohon Keluarga Berhasil Dihapus")
    else:
        messages.warning(request, "Pohon Keluarga Tidak Ada")

    return redirect(reverse('index'))

# https://codepen.io/Pestov/pen/BLpgm


def tree_detail(request):
    tree_id = request.GET.get('id', None)

    tree = Tree.objects.get(id=tree_id)
    keluarga = Person.objects.filter(tree=tree)
    keluarga_only_male = Person.objects.filter(tree=tree,gender='M')

    anggota_form = AnggotaForm(tree)
    search_form = ChoiceForm(id=tree_id)
    pasangan_form = PasanganForm(tree)
    response = {
        'tree': tree,
        'keluarga': keluarga,
        'keluarga_only_male': keluarga_only_male,
        'form': anggota_form,
        'forms': search_form,
        'spouse_form': pasangan_form
        }
    html = 'tree.html'
    return render(request, html, response)


def add_anggota(request):
    id = request.GET.get('id', None)
    tree = Tree.objects.get(id=id)

    if request.method == 'POST':
        # print(request.POST)
        father = Person.objects.get(tree=tree, name=request.POST['parent'])
        gender = 'female' if request.POST['gender'] == 'F' else 'male'

        if father is not None and father.generation < 3:
            new = Person.objects.create(name=request.POST['name'],
                                        father=father,
                                        mother=father.spouse,
                                        gender=request.POST['gender'],
                                        generation=father.generation + 1,
                                        tree=tree)
            new.save()

            sentence_save(new, gender, tree)
            sentence_save_(father, new, 'father', tree)
            sentence_save_(father.spouse, new, 'mother', tree)

            messages.success(request, "Anggota Keluarga Berhasil Ditambahkan")
        else:
            messages.warning(request, "Anggota Keluarga Tidak Berhasil Ditambahkan")
    else:
        messages.warning(request, "Anggota Keluarga Tidak Berhasil Ditambahkan")
    return redirect('/tree/?id={}'.format(id))

def add_anggota_pasangan(request):
    id = request.GET.get('id', None)

    if request.method == 'POST' and len(request.POST) > 3:
        # print(request.POST)
        tree = Tree.objects.get(id=id)
        spouse = Person.objects.get(tree=tree, name=request.POST['pasangan'])
        gender = 'female' if request.POST['gender'] == 'F' else 'male'

        if spouse is not None and spouse.gender != request.POST['gender']:
            new = Person.objects.create(name=request.POST['name'],
                                        spouse=spouse,
                                        gender=request.POST['gender'],
                                        generation=spouse.generation,
                                        tree=tree)
            new.save()
            spouse.spouse = new
            spouse.save()

            sentence_save(new, gender, tree)

            husband = sentence_save_(spouse, new, "husband", tree) if gender=='female'  else sentence_save_(new, spouse, "husband", tree)
            wife = sentence_save_(spouse, new, "wife", tree) if gender=='male'  else sentence_save_(new, spouse, "wife", tree)

            messages.success(request, "Anggota Keluarga Berhasil Ditambahkan")
        else:
            messages.warning(request, "Anggota Keluarga Tidak Berhasil Ditambahkan")
    else:
        messages.warning(request, "Anggota Keluarga Tidak Berhasil Ditambahkan")
    return redirect('/tree/?id={}'.format(id))

@csrf_exempt
def cari_relasi(request):
    id = request.GET.get('id', None)
    data = {}
    if request.method == 'POST':
        tree = Tree.objects.get(id=id)
        hasil = forward_chain(request.POST['orang_pertama'],
                            request.POST['orang_kedua'],
                            tree.id)
        #messages.success(request, hasil)
        data = {'hasil':hasil}
    return JsonResponse(data)
    #return redirect('/tree/?id={}'.format(id))

def sentence_save(person, link, tree):
    term = Term.objects.filter(link=link).first()
    person_sentence = Sentence.objects.create(
        person1=person, term=term, tree=tree)
    person_sentence.save()
    return person_sentence

def sentence_save_(person1, person2, link, tree):
    term = Term.objects.filter(link=link).first()
    sentence = Sentence.objects.create(
        person1=person1, person2=person2,
        term=term, tree=tree)
    sentence.save()
    return sentence
