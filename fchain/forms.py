from django import forms
from fchain.models import Term, Rule, Sentence, Person, Tree

CSS_CLASS = 'form-control bg-light'
REQUIRED_MSG = 'Harus diisi'

ORANG1_CHOICES = (
    (1, "Not relevant"),
    (2, "Review"),
    (3, "Maybe relevant"),
    (4, "Relevant"),
    (5, "Leading candidate")
)
ORANG2_CHOICES = (
    (1, "Unread"),
    (2, "Read")
)

class ChoiceForm(forms.Form):
    def __init__(self,*args,**kwargs):
        self.id = kwargs.pop('id')

        tree = Tree.objects.get(id=self.id)
        keluarga = Person.objects.filter(tree=tree)
        list_keluarga = []
        for i in keluarga:
            t_keluarga = (i.name,i.name)
            list_keluarga.append(t_keluarga)
        choices_list = tuple(list_keluarga)
        super(ChoiceForm,self).__init__(*args,**kwargs)
        self.fields['orang_pertama'].choices = choices_list
        self.fields['orang_kedua'].choices = choices_list

    orang_pertama = forms.ChoiceField(label="Nama orang satu",
                                initial='',
                                widget=forms.Select(attrs={'class':CSS_CLASS}),
                                required=True)

    orang_kedua = forms.ChoiceField(label="Nama orang dua",
                                initial='',
                                widget=forms.Select(attrs={'class':CSS_CLASS}),
                                required=True)
