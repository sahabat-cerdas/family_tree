from django import forms
from fchain.models import Term, Rule, Sentence, Person, Tree

GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
)

REQUIRED_MSG = 'Harus diisi'
CSS_CLASS = 'form-control bg-light'

class PasanganForm(forms.Form):

    def __init__(self, tree, *args, **kwargs):
        super(PasanganForm, self).__init__(*args, **kwargs)

        person = list(Person.objects.filter(tree=tree, spouse__isnull=True))
        orang = [ (x.name , x.name) for x in person ]
        orang = tuple(orang)

        self.fields['name'] = forms.CharField(label='Nama Anggota Pasangan',
                                   max_length=30,
                                   error_messages={
                                       'required': REQUIRED_MSG,
                                       'max_length': 'Nama pengguna maksimal 30 karakter'
                                   },
                                   widget=forms.TextInput(attrs={'class': CSS_CLASS,
                                                                 'placeholder': 'Masukkan Nama'}))

        self.fields['pasangan'] = forms.ChoiceField(label='Pasangan',
                                 choices=orang,
                                 error_messages={
                                     'required': REQUIRED_MSG,
                                     'invalid_choice': 'Pilihan tidak valid'
                                 },
                                 widget=forms.Select(attrs={'class': CSS_CLASS}))
        self.fields['gender'] = forms.ChoiceField(label='Tipe Gender',
                                 choices=GENDER_CHOICES,
                                 error_messages={
                                     'required': REQUIRED_MSG,
                                     'invalid_choice': 'Pilihan tidak valid'
                                 },
                                 widget=forms.Select(attrs={'class': CSS_CLASS}))
