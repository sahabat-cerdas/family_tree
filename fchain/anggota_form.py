from django import forms
from fchain.models import Term, Rule, Sentence, Person, Tree

GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
)

REQUIRED_MSG = 'Harus diisi'
CSS_CLASS = 'form-control bg-light'

class AnggotaForm(forms.Form):

    def __init__(self, tree, *args, **kwargs):
        super(AnggotaForm, self).__init__(*args, **kwargs)

        person = list(Person.objects.filter(tree=tree, gender='M',spouse__isnull=False, generation__lte=2))
        orang = [ (x.name , '{} dan {}'.format(x.name,x.spouse.name)) for x in person ]
        orang = tuple(orang)

        self.fields['name'] = forms.CharField(label='Nama Anggota Keluarga',
                                   max_length=30,
                                   error_messages={
                                       'required': REQUIRED_MSG,
                                       'max_length': 'Nama pengguna maksimal 30 karakter'
                                   },
                                   widget=forms.TextInput(attrs={'class': CSS_CLASS,
                                                                 'placeholder': 'Masukkan Nama'}))

        self.fields['parent'] = forms.ChoiceField(label='Orang Tua',
                                 choices=orang,
                                 error_messages={
                                     'required': REQUIRED_MSG,
                                     'invalid_choice': 'Pilihan tidak valid'
                                 },
                                 widget=forms.Select(attrs={'class': CSS_CLASS}))
        self.fields['gender'] = forms.ChoiceField(label='Tipe Gender',
                                 choices=GENDER_CHOICES,
                                 error_messages={
                                     'required': REQUIRED_MSG,
                                     'invalid_choice': 'Pilihan tidak valid'
                                 },
                                 widget=forms.Select(attrs={'class': CSS_CLASS}))
