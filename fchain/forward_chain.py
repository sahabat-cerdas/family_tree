from fchain.models import Term, Rule, Sentence, Person, Tree

# return string dan hanya string hubuungan antara 2 orang
# Depok 2018, All Right Reserved
# gak ngerti? tanya Aldi. -Aldi

def forward_chain(person_name1, person_name2, tree_id):
    search_person1 = Person.objects.filter(name=person_name1, tree=tree_id).first()
    search_person2 = Person.objects.filter(name=person_name2, tree=tree_id).first()
    tree = Tree.objects.filter(pk=tree_id).first()

    if not (search_person1 is None or search_person2 is None):
        if (search_person1.id == search_person2.id):
            return beutify(person_name1, person_name2, "same_person", tree_id)

        try:
            sentence = Sentence.objects.filter(
                person1=search_person1.id,
                person2=search_person2.id
            ).latest('pk')

            if sentence:
                return beutify(person_name1, person_name2, sentence, tree_id)
        except Sentence.DoesNotExist:
            pass

    rules = Rule.objects.all()
    for rule in rules:
        term1 = Term.objects.filter(pk=rule.term1.id).first()
        term2 = Term.objects.filter(pk=rule.term2.id).first()

        sentences_term1 = Sentence.objects.filter(term=term1.id, tree=tree_id)
        sentences_term2 = Sentence.objects.filter(term=term2.id, tree=tree_id)

        rule_type = rule.term1_alike

        if (rule_type != "special"):
            if not sentences_term1:
                continue
            for sentence1 in sentences_term1:

                if not sentences_term2:
                    continue
                for sentence2 in sentences_term2:

                    if sentence1.pk == sentence2.pk:
                        continue
                    
                    if (rule.term1_alike == "first"):
                        person1 = Person.objects.filter(pk=sentence1.person1.id, tree=tree_id).first()
                        person3 = Person.objects.filter(pk=sentence1.person2.id, tree=tree_id).first()
                    else:
                        person1 = Person.objects.filter(pk=sentence1.person2.id, tree=tree_id).first()
                        person3 = Person.objects.filter(pk=sentence1.person1.id, tree=tree_id).first()

                    if (rule.term2_alike == "first"):
                        person2 = Person.objects.filter(pk=sentence2.person1.id, tree=tree_id).first()
                        person4 = Person.objects.filter(pk=sentence2.person2.id, tree=tree_id).first()
                    else:
                        person2 = Person.objects.filter(pk=sentence2.person2.id, tree=tree_id).first()
                        person4 = Person.objects.filter(pk=sentence2.person1.id, tree=tree_id).first()

                    if (person1.pk == person2.pk):
                        new_sentence, created = Sentence.objects.get_or_create(
                            person1=person3,
                            person2=person4,
                            term=rule.conclusion,
                            tree=tree,
                        )
                        if created and (person3.pk == search_person2.pk and person4.pk == search_person1.pk):
                            new_sentence.save()
                            created_term = rule.conclusion.link
                            if (created_term == 'grandfather' or created_term == 'grandmother'):
                                return beutify(person_name1, person_name2, new_sentence, tree_id)

        else:
            if not sentences_term1:
                continue
            for sentence1 in sentences_term1:

                if not sentences_term2:
                    continue
                for sentence2 in sentences_term2:

                    if sentence1.pk == sentence2.pk:
                        continue

                    person1 = Person.objects.filter(pk=sentence1.person1.id, tree=tree_id).first()
                    person2 = Person.objects.filter(pk=sentence1.person2.id, tree=tree_id).first()

                    person3 = Person.objects.filter(pk=sentence2.person1.id, tree=tree_id).first()

                    if (person1.pk == person3.pk):
                        new_sentence, created = Sentence.objects.get_or_create(
                            person1=person1,
                            person2=person2,
                            term=rule.conclusion,
                            tree=tree,
                        )
                        if created and (person2.pk == search_person1.pk and person1.pk == search_person2.pk):
                            new_sentence.save()
                            return beutify(person1.name, person2.name, new_sentence, tree_id)
    
    try:
        sentence = Sentence.objects.filter(
            person1=search_person2.id,
            person2=search_person1.id
        ).latest('pk')

        if sentence:
            return beutify(person_name2, person_name1, sentence, tree_id)
    except Sentence.DoesNotExist:
        pass

    if not (search_person1 is None or search_person2 is None):
        if (search_person1.id == search_person2.id):
            return beutify(person_name1, person_name2, "same_person", tree_id)

        try:
            sentence = Sentence.objects.filter(
                person1=search_person1.id,
                person2=search_person2.id
            ).latest('pk')

            if sentence:
                return beutify(person_name1, person_name2, sentence, tree_id)
        except Sentence.DoesNotExist:
            pass    
    # kasus bukan satu keturunan dan bukan ipar dari siapa-siapa
    # dapat dibilang kasus ipar dari ipar (tapi ipar dari ipar terlalu general)
    return ("%s and %s doesn't have a closed family relation (doesn't have same ancestor and doesn't have in-law relation)." % (person_name1, person_name2))


def beutify(person_name1, person_name2, _sentence, tree_id):
    if _sentence != 'same_person':

        search_person1 = Person.objects.filter(name=person_name1, tree=tree_id).first()
        search_person2 = Person.objects.filter(name=person_name2, tree=tree_id).first()
        term = Term.objects.filter(link=_sentence).first()
        tree = Tree.objects.filter(pk=tree_id).first()

        if not (search_person1 is None or search_person2 is None):
            try:
                sentence = Sentence.objects.filter(
                    person1=search_person2.id,
                    person2=search_person1.id,
                    term=_sentence.term.id
                ).latest('pk')

                if sentence:
                    return beutify(person_name2, person_name1, _sentence, tree_id)
            except Sentence.DoesNotExist:
                pass

        link = _sentence.term.link.replace("_"," ")
    else:
        link = _sentence.replace("_"," ")
    return ("%s is the %s of %s!" % (person_name1, link, person_name2))