from django.db import models

# Create your models here.

# example: Father(x)


class Term(models.Model):

    link = models.CharField(max_length=50)

    def __str__(self):
        return self.link

# example: Father(nama1, nama2) & Father(nama2, nama3) -->
# Grandfather(nama1, nama3)


class Rule(models.Model):

    # term
    term1 = models.ForeignKey(
        Term, on_delete=models.CASCADE, related_name='rules_term1'
    )
    term2 = models.ForeignKey(
        Term, on_delete=models.CASCADE, related_name='rules_term2'
    )

    INDEX_CHOICES = (
        ('first', 0),
        ('second', 1),
        ('special', 2)
    )

    # what needs to be equal
    term1_alike = models.CharField(
        choices=INDEX_CHOICES,
        max_length=20
    )
    term2_alike = models.CharField(
        choices=INDEX_CHOICES,
        max_length=20
    )

    conclusion = models.ForeignKey(
        Term, on_delete=models.CASCADE, related_name='rules_conclusion'
    )


# family tree
class Tree(models.Model):

    name = models.CharField(max_length=50)
    desc = models.CharField(max_length=110, default=None)

    def __str__(self):
        return self.name

# Orang


class Person(models.Model):

    name = models.CharField(max_length=50)

    spouse = models.ForeignKey(
        "Person",
        on_delete=models.SET_NULL,
        related_name='spouse_of',
        null=True
    )
    father = models.ForeignKey(
        "Person",
        on_delete=models.CASCADE,
        related_name='child_father',
        null=True,
        blank=True,
    )
    mother = models.ForeignKey(
        "Person",
        on_delete=models.CASCADE,
        related_name='child_mother',
        null=True,
        blank=True,
    )
    tree = models.ForeignKey(
        Tree,
        on_delete=models.CASCADE,
        related_name='person',
        default=None
    )
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(
        max_length=1,
        choices=GENDER_CHOICES,
        default=None
    )
    generation = models.IntegerField(
        default=1
    )

    def __str__(self):
        return self.name


# example: Father(nama1, nama2) --> True
# always proven True


class Sentence(models.Model):

    # person
    person1 = models.ForeignKey(
        Person, on_delete=models.CASCADE, related_name='sentences_person1'
    )
    person2 = models.ForeignKey(
        Person, on_delete=models.CASCADE, related_name='sentences_person2',
        null=True, blank=True, default=None
    )

    # term
    term = models.ForeignKey(
        Term, on_delete=models.CASCADE, related_name='sentences'
    )

    is_displayed = models.BooleanField(default=False)

    tree = models.ForeignKey(
        Tree,
        on_delete=models.CASCADE,
        related_name='sentence',
        default=None
    )
